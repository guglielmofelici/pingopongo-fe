const urlParams = new URLSearchParams(window.location.search);

square1 = document.getElementById("square-1");
p1 = document.getElementById("player-1");

square2 = document.getElementById("square-2");
p2 = document.getElementById("player-2");

removeButton1 = document.getElementById("remove-point-btn-1");
removeButton2 = document.getElementById("remove-point-btn-2");

confirmSaveModal = document.getElementById("confirm-save-modal");

serve = document.getElementById("serve");
winner = undefined;
areSidesSwitched = false;
scoring = 21;
servesNumber = 5;
maxScore = 20;

async function initData() {
  winner = undefined;
  for (let p of [p1, p2]) {
    p.score = 0;
    p.adv = false;
  }
  if (!p1.serving && p2.serving) {
    switchServe();
  }
  p1.serving = true;
  p1.name = urlParams.get("p1");
  p2.name = urlParams.get("p2");
  if (urlParams.get("scoring") === "11") {
    scoring = 11;
    servesNumber = 2;
    maxScore = 10;
  }
  await Promise.all([
    fetch(`${baseUrl}/preview/${p1.name}/${p2.name}`)
      .then((data) => data.json())
      .then(([winnerChange, loserChange]) => {
        p1.winning_elo = winnerChange;
        p2.losing_elo = loserChange;
      }),

    fetch(`${baseUrl}/preview/${p2.name}/${p1.name}`)
      .then((data) => data.json())
      .then(([winnerChange, loserChange]) => {
        p2.winning_elo = winnerChange;
        p1.losing_elo = loserChange;
      }),
  ]);
  document.getElementById(
    "player-1-elo-change"
  ).innerText = `(+${p1.winning_elo}, ${p1.losing_elo})`;
  document.getElementById(
    "player-2-elo-change"
  ).innerText = `(+${p2.winning_elo}, ${p2.losing_elo})`;
}

function getOtherPlayer(p) {
  if (p === p1) {
    return p2;
  }
  return p1;
}

function switchServe() {
  p1.serving = !p1.serving;
  p2.serving = !p2.serving;
  serve.parentNode.removeChild(serve);
  [p1, p2].find((p) => p.serving).parentNode.appendChild(serve);
}

function switchSides() {
  slot1 = document.getElementById("slot-1");
  slot2 = document.getElementById("slot-2");
  slot1.style.float = slot1.style.float === "right" ? "left" : "right";
  slot2.style.float = slot2.style.float === "left" ? "right" : "left"; // invertiti cosi funziona anche in caso di float undefined
  areSidesSwitched = !areSidesSwitched;
}

function updateRemoveButtons() {
  if (winner === p2 || p2.adv || p1.score === 0) {
    removeButton1.disabled = true;
    // doesn't automatically drop focus in Firefox
    removeButton1.blur();
  } else {
    removeButton1.disabled = undefined;
  }
  if (winner === p1 || p1.adv || p2.score === 0) {
    removeButton2.disabled = true;
    removeButton2.blur();
  } else {
    removeButton2.disabled = undefined;
  }
}

function updateView() {
  serve.style.display = "block";
  if (winner) {
    serve.style.display = "none";
    winner.innerText = "W";
    getOtherPlayer(winner).innerText = "L";
  } else if ((p = [p1, p2].find((pl) => pl.adv))) {
    // Bad style ma comodo...
    p.innerText = "A";
    getOtherPlayer(p).innerText = "-";
  } else {
    p1.innerText = p1.score;
    p2.innerText = p2.score;
  }
  updateRemoveButtons();
  document.getElementById("save-game-btn").style.display = winner
    ? "block"
    : "none";
}

function assignPoint(p) {
  other = getOtherPlayer(p);
  if (winner) {
    return;
  }
  if (p.adv === true || (p.score === maxScore && other.score < maxScore)) {
    // vittoria
    p.score = maxScore + 1;
    winner = p;
    document.getElementById("save-game-btn").style.display = "block";
    p.adv = false;
  } else if (p.score === maxScore) {
    // vantaggi
    if (other.adv) {
      other.adv = false;
    } else {
      p.adv = true;
    }
    switchServe();
  } else {
    p.score++;
    if ((p1.score + p2.score) % servesNumber === 0) {
      switchServe();
    }
  }
  updateView();
}

function removePoint(p) {
  other = getOtherPlayer(p);
  if (other.adv || winner === other) {
    // Questi due casi sono proibiti
    return;
  }
  sum = p1.score + p2.score;
  if (sum > 0 && sum % servesNumber === 0) {
    switchServe();
  }
  if (winner === p) {
    p.score = maxScore;
    if (other.score === maxScore) {
      p.adv = true;
    }
    winner = undefined;
  } else if (p.adv) {
    p.adv = false;
  } else {
    p.score = Math.max(0, p.score - 1);
  }
  updateView();
}

function saveGame() {
  if (!winner) {
    return;
  }
  // document.getElementById('save-game-modal-text').innerHTML += `<br>Winner: ${winner.name} <br> Loser: ${getOtherPlayer(winner).name}`
  confirmSaveModal.style.display = "block";
}

function reset() {
  initData();
  updateView();
}

square1.onclick = () => assignPoint(p1);
square2.onclick = () => assignPoint(p2);

removeButton1.onclick = () => {
  removePoint(p1);
};
removeButton2.onclick = () => {
  removePoint(p2);
};
document.getElementById("save-game-btn").onclick = saveGame;

document.getElementById("confirm-save-no-btn").onclick = () =>
  (confirmSaveModal.style.display = "none");

document.getElementById("confirm-save-yes-btn").onclick = () => {
  // TODO obviously temporary, implement backend authentication
  const pw = document.getElementById("save-game-password").value;
  if (pw !== "SHREK") {
    alert("Wrong password");
  } else {
    window.location.href = `./index.html?winner=${winner.name}&loser=${
      getOtherPlayer(winner).name
    }`;
  }
};

document.getElementById("close-confirm-save-modal").onclick = () =>
  (confirmSaveModal.style.display = "none");

document.getElementById("swap-sides-btn").onclick = switchSides;
document.getElementById("swap-serve-btn").onclick = switchServe;
document.getElementById("reset-btn").onclick = reset;
for (elem of document.getElementsByClassName("player-name")) {
  elem.onclick = (event) => {
    event.stopImmediatePropagation();
  };
}

initData();
updateView();

window.addEventListener(
  "keydown",
  function (event) {
    if (event.key === "ArrowLeft") {
      assignPoint(areSidesSwitched ? p2 : p1);
    } else if (event.key === "ArrowRight") {
      assignPoint(areSidesSwitched ? p1 : p2);
    }
  },
  true
);

document.getElementById("p1-name").innerText = p1.name;
document.getElementById("p2-name").innerText = p2.name;
