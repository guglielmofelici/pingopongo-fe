const playersTableBody = document.getElementById("elo-list-body");
const matchesTable = document.getElementById("matches-table");
const matchesTableBody = document.getElementById("matches-table-body");
const addPlayerModal = document.getElementById("add-player-modal");
const confirmUndoModal = document.getElementById("confirm-undo-modal");
const successModal = document.getElementById("success-modal");

players = [];
// matches = [] // JSON.parse(sessionStorage.getItem('matches') || '[]')

/**** HELPERS ****/

function showElement(modal, shown = false) {
  modal.style.display = shown ? "block" : "none";
}

function successMessage(innerHTML = "Operation completed") {
  document.getElementById("success-modal-text").innerHTML = innerHTML;
  showElement(successModal);
}

function addOptionToSelect(
  select,
  optionText,
  optionValue = "",
  optionId = undefined,
  selected = false,
  disabled = false
) {
  let option = document.createElement("option");
  option.id = optionId;
  option.text = optionText;
  option.value = optionValue;
  option.selected = selected;
  option.disabled = disabled;
  select.add(option);
  option.dataset.originalText = option.textContent;
  return option;
}

// TODO come si usavano i default parameters? lol
function addDefaultSelectOptions(select) {
  addOptionToSelect(select, "", "", undefined, true);
  addOptionToSelect(select, "Add a new player", "#ADDPLAYER#");
  addOptionToSelect(select, "__________________", "", undefined, false, true);
  select.oninput = () =>
    select.value === "#ADDPLAYER#" && showElement(addPlayerModal, true);
}

function checkPw(str) {
  // TODO obviously temporary, implement backend authentication
  if (str !== "SHREK") {
    alert(str ? "Wrong password" : "Insert password");
    return false;
  }
  return true;
}

function addPlayer(name, elo = 1000) {
  if ((player = players.find((p) => p.name === name))) {
    player.elo = elo;
  } else {
    players.push({
      name,
      elo,
      isActive: true,
    });
  }
}

function styleDeleteBtn(deleteBtn) {
  deleteBtn.innerHTML = "X";
  Object.assign(deleteBtn.style, {
    color: "red",
    "font-weight": "bold",
    width: "7%",
    cursor: "pointer",
    ...deleteBtn.style,
  });
}

function updatePlayersTable() {
  // refresh playersTable sorting by elo
  playersTableBody.innerHTML = "";
  players.sort((a, b) => b.elo - a.elo);
  let i = 1;
  for (const player of players.filter((p) => p.isActive)) {
    let row = playersTableBody.insertRow();
    row.insertCell().innerHTML = i++;
    row.insertCell().innerHTML = player.name;
    row.insertCell().innerHTML = player.elo;
    fetch(`${baseUrl}/games/${player.name}`)
      .then((data) => data.json())
      .then((games) => {
        if (games.length < 2) {
          row.insertCell(); // Empty cell for styling purposes
          return;
        }
        const eloDifference = player.elo - games[1].elo;
        const isGain = eloDifference > 0;
        const color = isGain ? "green" : "red";
        const displayValue = isGain ? `+${eloDifference}` : eloDifference;

        row.insertCell().innerHTML = `<span style="color:${color}">${displayValue}</span>`;
      });
  }
}

function updateSelectOptions(
  keepSelections = false,
  lastAddedPlayer = undefined
) {
  // refresh options sorting by name
  players.sort((a, b) => (a.name > b.name ? 1 : b.name > a.name ? -1 : 0));
  for (const elem of document.getElementsByClassName("player-match-select")) {
    currSelection = elem.value;
    elem.innerHTML = "";
    addDefaultSelectOptions(elem);
    for (const player of players) {
      isSelected =
        (keepSelections && player.name === currSelection) ||
        (keepSelections &&
          currSelection === "#ADDPLAYER#" &&
          lastAddedPlayer === player.name);
      addOptionToSelect(elem, player.name, player.name, undefined, isSelected);
    }
  }
}

function updateMatchesTable() {
  // refresh matches
  matchesTableBody.innerHTML = "";
  fetch(`${baseUrl}/games/last/10`)
    .then((matches) => matches.json())
    .then((matches) => matches.reverse())
    .then((matches) => {
      matches.forEach((match, idx) => {
        let row = matchesTableBody.insertRow();
        deleteBtn = row.insertCell();
        if (idx === 0) {
          // can only delete last inserted match
          deleteBtn.onclick = () => {
            showElement(confirmUndoModal, true);
          };
          styleDeleteBtn(deleteBtn);
        }
        row.insertCell().innerHTML = match.winner;
        row.insertCell().innerHTML = match.loser;
      });
      matchesTable.style.display = matches.length ? "inline-table" : "none";
    })
    .catch((err) => alert("Errore nel recupero degli ultimi 5 match: " + err));
}

function fetchPlayers() {
  fetch(`${baseUrl}/getelo`)
    .then((list) => list.json())
    .then((data) => {
      players = data;
      updatePlayersTable();
      updateSelectOptions();
    })
    .catch((err) => alert("Errore nel recupero dei dati: " + err));
}

function addGame(winner, loser) {
  winner = winner || document.getElementById("winner").value;
  loser = loser || document.getElementById("loser").value;
  if (!winner || !loser) {
    alert("Insert winner and loser");
    return;
  }
  if (winner === loser) {
    alert("Winner and loser must be different");
    return;
  }
  fetch(`${baseUrl}/match/${winner}/${loser}`, { method: "POST" })
    .then(() => {
      fetchPlayers();
      // matches.unshift({ winner, loser })
      // sessionStorage.setItem('matches', JSON.stringify(matches))
      updateMatchesTable();
    })
    .catch((err) => alert("Errore nell'aggiunta del match: " + err));
}

function previewEloChange() {
  updateSelectOptions(true);
  winner = document.getElementById("winner").value;
  loser = document.getElementById("loser").value;
  if (winner && loser) {
    fetch(`${baseUrl}/preview/${winner}/${loser}`)
      .then((data) => data.json())
      .then(([winnerChange, loserChange]) => {
        [
          ["winner", winnerChange],
          ["loser", loserChange],
        ].forEach(([id, change]) => {
          // Get the select element
          const selectElement = document.getElementById(id);
          // Get the currently selected option element
          const selectedOption =
            selectElement.options[selectElement.selectedIndex];
          // Update the displayed text of the selected option
          selectedOption.textContent = selectedOption.textContent.concat(
            ` (${formatNumber(change)})`
          );
        });
      })
      .catch((err) => alert("Errore nel recupero dei dati: " + err));
  }
}

async function playGame() {
  p1 = document.getElementById("player-1").value;
  p2 = document.getElementById("player-2").value;
  if (!p1 || !p2) {
    alert("Insert both players");
    return;
  }
  if (p1 === p2) {
    alert("Players must be different");
    return;
  }
  scoring = document.getElementById("radio-11").checked ? 11 : 21;
  // const result = await Promise.all([
  //   fetch(`${baseUrl}/preview/${p1.name}/${p2.name}`).then((data) =>
  //     data.json()
  //   ),
  //   fetch(`${baseUrl}/preview/${p2.name}/${p1.name}`).then((data) =>
  //     data.json()
  //   ),
  // ]);
  // console.log(result);
  window.location.href = `./scorer.html?p1=${p1}&p2=${p2}&scoring=${scoring}`;
}

/**** CALLBACKS ****/

document.getElementById("play-game-btn").onclick = playGame;

document.getElementById("add-game-btn").onclick = () => {
  const pw = document.getElementById("add-game-password");
  if (checkPw(pw.value)) {
    pw.value = "";
    addGame();
  }
};

document.getElementById("confirm-undo-no-btn").onclick = () => {
  showElement(confirmUndoModal, false);
};

document.getElementById("confirm-undo-yes-btn").onclick = () => {
  const pw = document.getElementById("del-game-password");
  if (checkPw(pw.value)) {
    pw.value = "";
    fetch(`${baseUrl}/undo`, { method: "POST" })
      .then(() => {
        // matches.shift()
        // sessionStorage.setItem('matches', JSON.stringify(matches))
        updateMatchesTable();
        fetchPlayers();
      })
      .catch((err) => alert("Errore nell'undo: " + err));
    showElement(confirmUndoModal, false);
  }
};

document.getElementById("success-modal-ok-btn").onclick = () =>
  showElement(successModal, false);

document.getElementById("close-success-modal").onclick = () =>
  showElement(successModal, false);

document.getElementById("close-confirm-undo-modal").onclick = () =>
  showElement(confirmUndoModal, false);

document.getElementById("close-add-player-modal").onclick = () =>
  showElement(addPlayerModal, false);

document.getElementById("add-player-btn").onclick = () => {
  const name = document.getElementById("add-player-name-input").value;
  if (!name) {
    alert("Inserire un nome");
    return;
  }
  addPlayer(name);
  updateSelectOptions(true, name);
  updatePlayersTable();
  showElement(addPlayerModal, false);
};

document.getElementById("winner").onchange = previewEloChange;
document.getElementById("loser").onchange = previewEloChange;

// animate accordion
for (const acc of document.getElementsByClassName("accordion")) {
  acc.onclick = () => {
    acc.classList.toggle("active");
    const panel = acc.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  };
}

/**** MAIN ****/

for (const elem of document.getElementsByClassName("player-match-select")) {
  addDefaultSelectOptions(elem);
}

fetchPlayers();
updateMatchesTable();

const urlParams = new URLSearchParams(window.location.search);
if (urlParams.has("winner") && urlParams.has("loser")) {
  winner = urlParams.get("winner");
  loser = urlParams.get("loser");
  addGame(winner, loser);
  successMessage(`Added game.<br>Winner: ${winner}<br>Loser: ${loser}`);
  // delete query parameters after consuming them
  window.history.pushState(
    {},
    document.title,
    window.location.href.split("?")[0]
  );
}
