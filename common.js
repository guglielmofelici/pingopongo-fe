function formatNumber(num) {
  return num >= 0 ? `+${num}` : num.toString();
}

window.formatNumber = formatNumber;
//window.baseUrl = "http://localhost:8000/api/v1";
window.baseUrl = "https://api.guglielmofelici.com/pingopongo/api/v1";
